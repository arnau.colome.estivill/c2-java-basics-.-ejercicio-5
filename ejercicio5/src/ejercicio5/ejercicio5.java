package ejercicio5;

public class ejercicio5 {

	public static void main(String[] args) {
		/*
			5 - Programa java que declare cuatro variables enteras A, B, C y D y
			asígnale un valor a cada una. A continuación realiza las instrucciones
			necesarias para que:
			• B tome el valor de C
			• C tome el valor de A
			• A tome el valor de D
			• D tome el valor de B
		*/

		int A = 10;
		
		int B = 6;
		
		int C = 15;
		
		int D = 7;
		
		B = C;
		System.out.println("B toma el valor de C: " + B);
		
		C = A;
		System.out.println("C toma el valor de A: " + C);
		
		A = D;
		System.out.println("A toma el valor de D: " + A);
		
		D = B;
		System.out.println("D toma el valor de B: " + B);
		
	}

}
