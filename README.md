# C2 Java Basics . Ejercicio 5

5 - Programa java que declare cuatro variables enteras A, B, C y D y
asígnale un valor a cada una. A continuación realiza las instrucciones
necesarias para que:
• B tome el valor de C
• C tome el valor de A
• A tome el valor de D
• D tome el valor de B